Object firebolt
type 102
subtype 4
face firebolt.111
animation firebolt
is_animated 0
is_turnable 1
move_type fly_low
speed 1
glow_radius 2
no_pick 1
weight 500
smoothlevel 100
smoothface firebolt.111 firebolt_S.111
smoothface firebolt.121 firebolt_S.111
smoothface firebolt.131 firebolt_S.111
smoothface firebolt.141 firebolt_S.111
smoothface firebolt.151 firebolt_S.111
smoothface firebolt.161 firebolt_S.111
smoothface firebolt.171 firebolt_S.111
smoothface firebolt.181 firebolt_S.111
end
