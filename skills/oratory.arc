Object skill_oratory
name oratory
skill oratory
type 43
invisible 1
no_drop 1
subtype 12
body_skill -1
exp 10
level 250
msg
A sufficiently compelling speech can convince the audience to join you in your travels! It won't convince anyone who's already hostile to calm down, though.
Active (Int,Cha): Attempt to convince a non-hostile creature to join your party.
endmsg
end
